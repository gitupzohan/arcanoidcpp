// Fill out your copyright notice in the Description page of Project Settings.


#include "Arcanoid/Ball/Ball.h"
#include "Arcanoid/Game/ArcanoidGameMode.h"
#include "Arcanoid/Game/ArcanoidGameState.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/KillZVolume.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABall::ABall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create mesh component
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->OnComponentHit.AddDynamic(this, &ABall::SphereHit);
	Mesh->OnComponentBeginOverlap.AddDynamic(this, &ABall::BallBeginOverlap);
	RootComponent = Mesh;
	Mesh->SetCanEverAffectNavigation(false);

}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();
	SetRandomDirection();
	auto GameState = GetWorld()->GetGameState<AArcanoidGameState>();
	if (GameState)
	{
		GameState->PlayingBall = this;
		GameState->AllBalls.Add(this);
	}
}

// Called every frame
void ABall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	MovementBall(DeltaTime);
}

void ABall::SpawnBonusBall(UClass* Ball)
{
	for (int i = 0; i < 3; i++)
	{
		FVector SpawnLocation = Mesh->GetRelativeLocation();
		FRotator SpawnRotation = GetActorRotation();
		FActorSpawnParameters SpawnParam;
		SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParam.Owner = this;
		SpawnParam.Instigator = GetInstigator();
		GetWorld()->SpawnActor<ABall>(Ball, SpawnLocation, SpawnRotation, SpawnParam);
	}
	
}

void ABall::SetRandomDirection()
{
	FVector ClampVector(100.0f, FMath::RandRange(-100.0f, 100.0f), 0);
	Direction = UKismetMathLibrary::ClampVectorSize(ClampVector, 100.0f, 100.0f);
}

void ABall::DecrementSpeed()
{
	Speed -= PercentSpeed;
}

void ABall::IncrementSpeed()
{
	Speed += PercentSpeed;
}

void ABall::MovementBall(float DeltaSeconds)
{
	Mesh->AddWorldOffset(Speed * DeltaSeconds * Direction, true);
}

void ABall::SphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (Hit.bBlockingHit)
	{
		FVector ReflectionVector = UKismetMathLibrary::GetReflectionVector(Direction, Hit.Normal);
		Direction.Set(ReflectionVector.X, ReflectionVector.Y, 0);
	}
}

void ABall::BallBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AKillZVolume* KillZVolume = Cast<AKillZVolume>(OtherActor);
	if (KillZVolume)
	{
		auto GameState = GetWorld()->GetGameState<AArcanoidGameState>();
		if (GameState)
		{
			GameState->RemoveBall(this);
		}
		Destroy();
	}
}


//ChildExploseBall============================================//

void AExploseBall::BeginPlay()
{
	Super::BeginPlay();
}

void AExploseBall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AExploseBall::Explose()
{
	TArray<AActor*> IgnoreActors;
	UGameplayStatics::ApplyRadialDamage(GetWorld(), 1.0f, GetActorLocation(), DamageRadius, NULL, IgnoreActors, this, nullptr);
	Destroy();
}

void AExploseBall::SphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//if (OtherActor == Block)
	//{
	//
	//}
	//else
	//Super::SphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
	if (Hit.bBlockingHit)
	{
		Explose();
	}
}

void AExploseBall::BallBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}
