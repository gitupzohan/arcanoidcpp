// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Ball.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDead,int,Health);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSpawn);

UCLASS()
class ARCANOID_API ABall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABall();

	FOnDead OnDead;
	FOnSpawn OnSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		UStaticMeshComponent* Mesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FVector Direction = FVector();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float Speed = 25.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float PercentSpeed = 2.0f;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//functions
	UFUNCTION(BlueprintCallable)
		void SpawnBonusBall(UClass* Ball);
	UFUNCTION()
		void SetRandomDirection();
	UFUNCTION()
		void DecrementSpeed();
	UFUNCTION()
		void IncrementSpeed();
	UFUNCTION()
		void MovementBall(float DeltaSeconds);
	UFUNCTION()
		virtual void SphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	UFUNCTION()
		virtual void BallBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};

UCLASS()
class ARCANOID_API AExploseBall : public ABall
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explose")
		float DamageRadius = 300.0f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//functions
	UFUNCTION()
		void Explose();

	virtual void SphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	virtual void BallBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
