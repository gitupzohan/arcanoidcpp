// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/SphereComponent.h"
#include "Arcanoid/Ball/Ball.h"
#include "Racket.generated.h"

UCLASS()
class ARCANOID_API ARacket : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ARacket();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		UStaticMeshComponent* Mesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnBallPoint")
		USphereComponent* SpawnBallPoint = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float Speed = 30.0f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void MovePawn(float AxisValue);
	ABall* SpawnBall();
	void SpawnExploseBall();

};
