// Fill out your copyright notice in the Description page of Project Settings.


#include "Arcanoid/Racket/Racket.h"
#include "Arcanoid/Game/ArcanoidPlayerController.h"
#include "Arcanoid/Game/ArcanoidGameMode.h"
#include "Arcanoid/Game/ArcanoidGameState.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ARacket::ARacket()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = Mesh;

	SpawnBallPoint = CreateDefaultSubobject<USphereComponent>(TEXT("SpawnBallPoint"));
	SpawnBallPoint->SetupAttachment(RootComponent);

	AutoPossessPlayer = EAutoReceiveInput::Player0;
}

// Called when the game starts or when spawned
void ARacket::BeginPlay()
{
	Super::BeginPlay();
	auto GameState = GetWorld()->GetGameState<AArcanoidGameState>();
	if(GameState)
		GameState->Racket = this;
	SpawnBall();
}

// Called every frame
void ARacket::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ARacket::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis("MoveRight", this, &ARacket::MovePawn);
	InputComponent->BindAction("ExploseBall",EInputEvent::IE_Pressed, this, &ARacket::SpawnExploseBall);
}

void ARacket::MovePawn(float AxisValue)
{
	FVector Direction(0,AxisValue* GetWorld()->DeltaTimeSeconds * Speed * 100,0);
	AddActorWorldOffset(Direction, true);

}

ABall* ARacket::SpawnBall()
{
	AArcanoidGameMode* MyGameMode = Cast<AArcanoidGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (MyGameMode)
	{
		FVector SpawnLocation = SpawnBallPoint->GetComponentLocation();
		FRotator SpawnRotation = SpawnBallPoint->GetComponentRotation();
		FActorSpawnParameters SpawnParam;
		SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParam.Owner = this;
		SpawnParam.Instigator = GetInstigator();
		ABall* MyBall = Cast<ABall>(GetWorld()->SpawnActor<ABall>(MyGameMode->PlayingBall, SpawnLocation, SpawnRotation, SpawnParam));
		if (MyBall)
		{
			return MyBall;
		}
	}
	return nullptr;
}

void ARacket::SpawnExploseBall()
{
	AArcanoidGameMode* MyGameMode = Cast<AArcanoidGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (MyGameMode)
	{
		FVector SpawnLocation = SpawnBallPoint->GetComponentLocation();
		FRotator SpawnRotation = SpawnBallPoint->GetComponentRotation();
		FActorSpawnParameters SpawnParam;
		SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParam.Owner = this;
		SpawnParam.Instigator = GetInstigator();
		GetWorld()->SpawnActor<ABall>(MyGameMode->ExploseBall, SpawnLocation, SpawnRotation, SpawnParam);
	}
}

