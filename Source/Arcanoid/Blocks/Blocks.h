// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "Materials/MaterialInterface.h"
#include "Arcanoid/Blocks/BlockBase.h"
#include "Arcanoid/Game/ArcanoidGameMode.h"
#include "Arcanoid/Game/ArcanoidGameState.h"
#include "Blocks.generated.h"

class AArcanoidGameState;
class AArcanoidGameMode;
class USceneComponent;
class UHierarchicalInstancedStaticMeshComponent;

UCLASS()
class ARCANOID_API ABlocks : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlocks();
		
	TArray<int32> Instances;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Root")
		UHierarchicalInstancedStaticMeshComponent* HierarchicalInstanced;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		UMaterialInterface* Material = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		FVector BlockScale = FVector(1.0f);
		FVector GenerateLocation = FVector(1.0f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ConstructorBlocks")
		int CoutX = 2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ConstructorBlocks")
		int CoutY = 2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ConstructorBlocks")
		float Padding = 120.0f;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void DesignGenerateBlocks();
	void SpawnBlocks();
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

};

