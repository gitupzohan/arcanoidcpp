// Fill out your copyright notice in the Description page of Project Settings.


#include "Arcanoid/Blocks/BlockBase.h"

// Sets default values
ABlockBase::ABlockBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Block"));
	Mesh->SetupAttachment(RootComponent);
	Mesh->OnComponentHit.AddDynamic(this, &ABlockBase::BlockHit);
}

// Called when the game starts or when spawned
void ABlockBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlockBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlockBase::BlockHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	ABall* HitBall = Cast<ABall>(OtherActor);
	if (HitBall)
	{
		Destroy();
	}
}
// Undestructable block==============================================================
void AUndestructBlock::BeginPlay()
{
	Super::BeginPlay();
}

void AUndestructBlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AUndestructBlock::BlockHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{

}

// Bomb block=========================================================================
void ABombBlock::BeginPlay()
{
	Super::BeginPlay();
}

void ABombBlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABombBlock::BlockHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{

}
