// Fill out your copyright notice in the Description page of Project Settings.


#include "Arcanoid/Blocks/Blocks.h"
#include "Kismet/KismetStringLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABlocks::ABlocks()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	HierarchicalInstanced = CreateDefaultSubobject<UHierarchicalInstancedStaticMeshComponent>(TEXT("HierarchicalInstanced"));
	HierarchicalInstanced->SetupAttachment(RootComponent);
	GenerateLocation = GetActorLocation();

}

// Called when the game starts or when spawned
void ABlocks::BeginPlay()
{
	Super::BeginPlay();
	SpawnBlocks();
}

// Called every frame
void ABlocks::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlocks::DesignGenerateBlocks()
{
	HierarchicalInstanced->ClearInstances();
	for (int x = 0; x < CoutX; x++)
	{
		for (int y = 0; y < CoutY; y++)
		{
			//FString NameString = FString(TEXT("Block"));
			//FName Name = UKismetStringLibrary::Conv_StringToName(UKismetStringLibrary::Concat_StrStr(NameString, UKismetStringLibrary::Conv_IntToString(Number)));
			FVector RelativeLocation(x * Padding, y * Padding, 0);
			FTransform Transform = UKismetMathLibrary::MakeTransform(GenerateLocation + RelativeLocation, FRotator(0), BlockScale);
			Instances.Add(HierarchicalInstanced->AddInstance(Transform));
			HierarchicalInstanced->SetMaterial(0,Material);
		}	
	}
}

void ABlocks::SpawnBlocks()
{
	for (int i = 0; i < HierarchicalInstanced->GetNumRenderInstances(); i++)
	{
		FTransform Transform;
		HierarchicalInstanced->GetInstanceTransform(i, Transform, true);
		AArcanoidGameMode* MyGameMode = Cast<AArcanoidGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		if (MyGameMode)
		{
			FActorSpawnParameters SpawnParam;
			SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParam.Owner = this;
			SpawnParam.Instigator = GetInstigator();
			ABlockBase* MyBlock = Cast<ABlockBase>(GetWorld()->SpawnActor<ABlockBase>(MyGameMode->Block, Transform, SpawnParam));
			if (MyBlock)
			{
				MyBlock->Mesh->SetMaterial(0, Material);
			}
		}
	}	
	HierarchicalInstanced->DestroyComponent();
}

void ABlocks::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);
	DesignGenerateBlocks();
}

