// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "Arcanoid/Ball/Ball.h"
#include "Arcanoid/Racket/Racket.h"
#include "Arcanoid/Blocks/BlockBase.h"
#include "ArcanoidGameMode.generated.h"

UCLASS(minimalapi)
class AArcanoidGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AArcanoidGameMode();

	//property
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Actors")
		TSubclassOf<ABall> PlayingBall;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Actors")
		TSubclassOf<AExploseBall> ExploseBall;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Actors")
		TSubclassOf<ABlockBase> Block;
	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = "Special")
		int BombMaxCout = 3;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Special")
		int BallMaxCout = 3;

	//functions
	UFUNCTION()
		void SpawnPlayingBall();
	UFUNCTION()
		bool CanSpawnBall();
	UFUNCTION()
		bool CanSpawnExploseBall();
protected:
	virtual void BeginPlay() override;
};



