// Fill out your copyright notice in the Description page of Project Settings.


#include "Arcanoid/Game/ArcanoidPlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Arcanoid/Racket/Racket.h"
#include "Camera/CameraActor.h"

void AArcanoidPlayerController::BeginPlay()
{
	Super::BeginPlay();

	//CameraBlend
	SetViewTargetWithBlend(UGameplayStatics::GetActorOfClass(GetWorld(),ACameraActor::StaticClass()));
}

void AArcanoidPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
}

void AArcanoidPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

}

void AArcanoidPlayerController::SetNewMoveDestination(const FVector DestLocation)
{
}

void AArcanoidPlayerController::OnSetDestinationPressed()
{
}

void AArcanoidPlayerController::OnSetDestinationReleased()
{
}

void AArcanoidPlayerController::OnUnPossess()
{
}
