// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "Arcanoid/Ball/Ball.h"
#include "Arcanoid/Racket/Racket.h"
#include "ArcanoidGameState.generated.h"

/**
 * 
 */
UCLASS()
class ARCANOID_API AArcanoidGameState : public AGameState
{
	GENERATED_BODY()
	
public:

	AArcanoidGameState();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<ABall*> AllBalls;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int BombCurrentCout = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Health = 5;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Score = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Actors")
		ABall* PlayingBall = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Actors")
		ARacket* Racket = nullptr;

	UFUNCTION()
		void RemoveBall(ABall* Ball);
	UFUNCTION()
		void IncrementHealth(int Value);
	UFUNCTION()
		void IncrementScore(int Value);
	UFUNCTION()
		void IncrementBomb(int Value);
protected:
	virtual void BeginPlay() override;
};
