// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ArcanoidPlayerController.generated.h"

/**
 * 
 */

UCLASS()
class ARCANOID_API AArcanoidPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	

protected:
	
	virtual void BeginPlay() override;

	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;

	/** Navigate player to the given world location. */
	void SetNewMoveDestination(const FVector DestLocation);

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();

	virtual void OnUnPossess()override;
};
