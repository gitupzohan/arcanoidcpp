// Fill out your copyright notice in the Description page of Project Settings.


#include "Arcanoid/Game/ArcanoidGameState.h"
#include "Arcanoid/Game/ArcanoidGameMode.h"
#include "Kismet/GameplayStatics.h"

AArcanoidGameState::AArcanoidGameState()
{

}

void AArcanoidGameState::RemoveBall(ABall* Ball)
{
	//AArcanoidGameMode* GameMode1 = Cast<AArcanoidGameMode>(GetWorld()->GetAuthGameMode());
	AllBalls.Remove(Ball);
	AArcanoidGameMode* GameMode = Cast<AArcanoidGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (GameMode)
	{
		if(GameMode->CanSpawnBall())

			GameMode->SpawnPlayingBall();
	}
}

void AArcanoidGameState::IncrementHealth(int Value)
{
	Health += Value;
}

void AArcanoidGameState::IncrementScore(int Value)
{
	Score += Value;
}

void AArcanoidGameState::IncrementBomb(int Value)
{
	BombCurrentCout += Value;
}

void AArcanoidGameState::BeginPlay()
{
	Super::BeginPlay();
	
}
