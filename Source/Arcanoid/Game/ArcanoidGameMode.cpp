// Copyright Epic Games, Inc. All Rights Reserved.

#include "ArcanoidGameMode.h"
#include "UObject/ConstructorHelpers.h"
#include "Arcanoid/Game/ArcanoidPlayerController.h"
#include "Arcanoid/Game/ArcanoidGameState.h"
#include "Kismet/GameplayStatics.h"

AArcanoidGameMode::AArcanoidGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AArcanoidPlayerController::StaticClass();
	GameStateClass = AArcanoidGameState::StaticClass();
	// set default pawn class to our Blueprinted character
	DefaultPawnClass = ARacket::StaticClass();
	
}

void AArcanoidGameMode::SpawnPlayingBall()
{
	AArcanoidGameState* MyGameState = Cast<AArcanoidGameState>(UGameplayStatics::GetGameState(GetWorld()));
	if (MyGameState)
	{
		MyGameState->Racket->SpawnBall();
	}	
}

bool AArcanoidGameMode::CanSpawnBall()
{
	AArcanoidGameState* MyGameState = Cast<AArcanoidGameState>(UGameplayStatics::GetGameState(GetWorld()));
	if (MyGameState)
	{
		if (MyGameState->AllBalls.Num() == 0 && MyGameState->Health > 1)
		{
			MyGameState->IncrementHealth(-1);
			return true;
		}
	}
	return false;
}

bool AArcanoidGameMode::CanSpawnExploseBall()
{
	AArcanoidGameState* MyGameState = Cast<AArcanoidGameState>(UGameplayStatics::GetGameState(GetWorld()));
	if (MyGameState)
	{
		if (MyGameState->BombCurrentCout > 0)
			return true;
	}
	return false;
}

void AArcanoidGameMode::BeginPlay()
{
	Super::BeginPlay();
}
