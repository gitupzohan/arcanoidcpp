// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARCANOID_Blocks_generated_h
#error "Blocks.generated.h already included, missing '#pragma once' in Blocks.h"
#endif
#define ARCANOID_Blocks_generated_h

#define Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_SPARSE_DATA
#define Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_RPC_WRAPPERS
#define Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABlocks(); \
	friend struct Z_Construct_UClass_ABlocks_Statics; \
public: \
	DECLARE_CLASS(ABlocks, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(ABlocks)


#define Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_INCLASS \
private: \
	static void StaticRegisterNativesABlocks(); \
	friend struct Z_Construct_UClass_ABlocks_Statics; \
public: \
	DECLARE_CLASS(ABlocks, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(ABlocks)


#define Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABlocks(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABlocks) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABlocks); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABlocks); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABlocks(ABlocks&&); \
	NO_API ABlocks(const ABlocks&); \
public:


#define Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABlocks(ABlocks&&); \
	NO_API ABlocks(const ABlocks&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABlocks); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABlocks); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABlocks)


#define Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_PRIVATE_PROPERTY_OFFSET
#define Arcanoid_Source_Arcanoid_Blocks_Blocks_h_20_PROLOG
#define Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_RPC_WRAPPERS \
	Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_INCLASS \
	Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_INCLASS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Blocks_Blocks_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARCANOID_API UClass* StaticClass<class ABlocks>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcanoid_Source_Arcanoid_Blocks_Blocks_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
