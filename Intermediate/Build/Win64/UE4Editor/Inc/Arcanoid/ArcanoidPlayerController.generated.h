// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARCANOID_ArcanoidPlayerController_generated_h
#error "ArcanoidPlayerController.generated.h already included, missing '#pragma once' in ArcanoidPlayerController.h"
#endif
#define ARCANOID_ArcanoidPlayerController_generated_h

#define Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_SPARSE_DATA
#define Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_RPC_WRAPPERS
#define Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAArcanoidPlayerController(); \
	friend struct Z_Construct_UClass_AArcanoidPlayerController_Statics; \
public: \
	DECLARE_CLASS(AArcanoidPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(AArcanoidPlayerController)


#define Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAArcanoidPlayerController(); \
	friend struct Z_Construct_UClass_AArcanoidPlayerController_Statics; \
public: \
	DECLARE_CLASS(AArcanoidPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(AArcanoidPlayerController)


#define Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AArcanoidPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AArcanoidPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArcanoidPlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArcanoidPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArcanoidPlayerController(AArcanoidPlayerController&&); \
	NO_API AArcanoidPlayerController(const AArcanoidPlayerController&); \
public:


#define Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AArcanoidPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArcanoidPlayerController(AArcanoidPlayerController&&); \
	NO_API AArcanoidPlayerController(const AArcanoidPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArcanoidPlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArcanoidPlayerController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AArcanoidPlayerController)


#define Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_PRIVATE_PROPERTY_OFFSET
#define Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_13_PROLOG
#define Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_RPC_WRAPPERS \
	Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_INCLASS \
	Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_INCLASS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARCANOID_API UClass* StaticClass<class AArcanoidPlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcanoid_Source_Arcanoid_Game_ArcanoidPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
