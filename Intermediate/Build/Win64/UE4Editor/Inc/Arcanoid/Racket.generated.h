// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARCANOID_Racket_generated_h
#error "Racket.generated.h already included, missing '#pragma once' in Racket.h"
#endif
#define ARCANOID_Racket_generated_h

#define Arcanoid_Source_Arcanoid_Racket_Racket_h_14_SPARSE_DATA
#define Arcanoid_Source_Arcanoid_Racket_Racket_h_14_RPC_WRAPPERS
#define Arcanoid_Source_Arcanoid_Racket_Racket_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Arcanoid_Source_Arcanoid_Racket_Racket_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARacket(); \
	friend struct Z_Construct_UClass_ARacket_Statics; \
public: \
	DECLARE_CLASS(ARacket, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(ARacket)


#define Arcanoid_Source_Arcanoid_Racket_Racket_h_14_INCLASS \
private: \
	static void StaticRegisterNativesARacket(); \
	friend struct Z_Construct_UClass_ARacket_Statics; \
public: \
	DECLARE_CLASS(ARacket, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(ARacket)


#define Arcanoid_Source_Arcanoid_Racket_Racket_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARacket(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARacket) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARacket); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARacket); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARacket(ARacket&&); \
	NO_API ARacket(const ARacket&); \
public:


#define Arcanoid_Source_Arcanoid_Racket_Racket_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARacket(ARacket&&); \
	NO_API ARacket(const ARacket&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARacket); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARacket); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARacket)


#define Arcanoid_Source_Arcanoid_Racket_Racket_h_14_PRIVATE_PROPERTY_OFFSET
#define Arcanoid_Source_Arcanoid_Racket_Racket_h_11_PROLOG
#define Arcanoid_Source_Arcanoid_Racket_Racket_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Racket_Racket_h_14_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Racket_Racket_h_14_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Racket_Racket_h_14_RPC_WRAPPERS \
	Arcanoid_Source_Arcanoid_Racket_Racket_h_14_INCLASS \
	Arcanoid_Source_Arcanoid_Racket_Racket_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcanoid_Source_Arcanoid_Racket_Racket_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Racket_Racket_h_14_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Racket_Racket_h_14_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Racket_Racket_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Racket_Racket_h_14_INCLASS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Racket_Racket_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARCANOID_API UClass* StaticClass<class ARacket>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcanoid_Source_Arcanoid_Racket_Racket_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
