// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
struct FVector;
class UObject;
#ifdef ARCANOID_Ball_generated_h
#error "Ball.generated.h already included, missing '#pragma once' in Ball.h"
#endif
#define ARCANOID_Ball_generated_h

#define Arcanoid_Source_Arcanoid_Ball_Ball_h_10_DELEGATE \
static inline void FOnSpawn_DelegateWrapper(const FMulticastScriptDelegate& OnSpawn) \
{ \
	OnSpawn.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Arcanoid_Source_Arcanoid_Ball_Ball_h_9_DELEGATE \
struct _Script_Arcanoid_eventOnDead_Parms \
{ \
	int32 Health; \
}; \
static inline void FOnDead_DelegateWrapper(const FMulticastScriptDelegate& OnDead, int32 Health) \
{ \
	_Script_Arcanoid_eventOnDead_Parms Parms; \
	Parms.Health=Health; \
	OnDead.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Arcanoid_Source_Arcanoid_Ball_Ball_h_15_SPARSE_DATA
#define Arcanoid_Source_Arcanoid_Ball_Ball_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execBallBeginOverlap); \
	DECLARE_FUNCTION(execSphereHit); \
	DECLARE_FUNCTION(execMovementBall); \
	DECLARE_FUNCTION(execIncrementSpeed); \
	DECLARE_FUNCTION(execDecrementSpeed); \
	DECLARE_FUNCTION(execSetRandomDirection); \
	DECLARE_FUNCTION(execSpawnBonusBall);


#define Arcanoid_Source_Arcanoid_Ball_Ball_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execBallBeginOverlap); \
	DECLARE_FUNCTION(execSphereHit); \
	DECLARE_FUNCTION(execMovementBall); \
	DECLARE_FUNCTION(execIncrementSpeed); \
	DECLARE_FUNCTION(execDecrementSpeed); \
	DECLARE_FUNCTION(execSetRandomDirection); \
	DECLARE_FUNCTION(execSpawnBonusBall);


#define Arcanoid_Source_Arcanoid_Ball_Ball_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABall(); \
	friend struct Z_Construct_UClass_ABall_Statics; \
public: \
	DECLARE_CLASS(ABall, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(ABall)


#define Arcanoid_Source_Arcanoid_Ball_Ball_h_15_INCLASS \
private: \
	static void StaticRegisterNativesABall(); \
	friend struct Z_Construct_UClass_ABall_Statics; \
public: \
	DECLARE_CLASS(ABall, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(ABall)


#define Arcanoid_Source_Arcanoid_Ball_Ball_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABall(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABall) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABall); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABall); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABall(ABall&&); \
	NO_API ABall(const ABall&); \
public:


#define Arcanoid_Source_Arcanoid_Ball_Ball_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABall(ABall&&); \
	NO_API ABall(const ABall&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABall); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABall); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABall)


#define Arcanoid_Source_Arcanoid_Ball_Ball_h_15_PRIVATE_PROPERTY_OFFSET
#define Arcanoid_Source_Arcanoid_Ball_Ball_h_12_PROLOG
#define Arcanoid_Source_Arcanoid_Ball_Ball_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_15_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_15_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_15_RPC_WRAPPERS \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_15_INCLASS \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcanoid_Source_Arcanoid_Ball_Ball_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_15_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_15_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_15_INCLASS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARCANOID_API UClass* StaticClass<class ABall>();

#define Arcanoid_Source_Arcanoid_Ball_Ball_h_60_SPARSE_DATA
#define Arcanoid_Source_Arcanoid_Ball_Ball_h_60_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execExplose);


#define Arcanoid_Source_Arcanoid_Ball_Ball_h_60_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execExplose);


#define Arcanoid_Source_Arcanoid_Ball_Ball_h_60_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAExploseBall(); \
	friend struct Z_Construct_UClass_AExploseBall_Statics; \
public: \
	DECLARE_CLASS(AExploseBall, ABall, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(AExploseBall)


#define Arcanoid_Source_Arcanoid_Ball_Ball_h_60_INCLASS \
private: \
	static void StaticRegisterNativesAExploseBall(); \
	friend struct Z_Construct_UClass_AExploseBall_Statics; \
public: \
	DECLARE_CLASS(AExploseBall, ABall, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(AExploseBall)


#define Arcanoid_Source_Arcanoid_Ball_Ball_h_60_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AExploseBall(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AExploseBall) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AExploseBall); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AExploseBall); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AExploseBall(AExploseBall&&); \
	NO_API AExploseBall(const AExploseBall&); \
public:


#define Arcanoid_Source_Arcanoid_Ball_Ball_h_60_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AExploseBall() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AExploseBall(AExploseBall&&); \
	NO_API AExploseBall(const AExploseBall&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AExploseBall); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AExploseBall); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AExploseBall)


#define Arcanoid_Source_Arcanoid_Ball_Ball_h_60_PRIVATE_PROPERTY_OFFSET
#define Arcanoid_Source_Arcanoid_Ball_Ball_h_57_PROLOG
#define Arcanoid_Source_Arcanoid_Ball_Ball_h_60_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_60_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_60_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_60_RPC_WRAPPERS \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_60_INCLASS \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_60_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcanoid_Source_Arcanoid_Ball_Ball_h_60_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_60_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_60_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_60_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_60_INCLASS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Ball_Ball_h_60_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARCANOID_API UClass* StaticClass<class AExploseBall>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcanoid_Source_Arcanoid_Ball_Ball_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
