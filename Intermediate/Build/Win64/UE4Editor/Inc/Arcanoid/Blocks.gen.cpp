// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Arcanoid/Blocks/Blocks.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBlocks() {}
// Cross Module References
	ARCANOID_API UClass* Z_Construct_UClass_ABlocks_NoRegister();
	ARCANOID_API UClass* Z_Construct_UClass_ABlocks();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Arcanoid();
	ENGINE_API UClass* Z_Construct_UClass_UHierarchicalInstancedStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void ABlocks::StaticRegisterNativesABlocks()
	{
	}
	UClass* Z_Construct_UClass_ABlocks_NoRegister()
	{
		return ABlocks::StaticClass();
	}
	struct Z_Construct_UClass_ABlocks_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HierarchicalInstanced_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HierarchicalInstanced;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Material_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Material;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlockScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BlockScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CoutX_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_CoutX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CoutY_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_CoutY;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Padding_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Padding;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABlocks_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Arcanoid,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABlocks_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Blocks/Blocks.h" },
		{ "ModuleRelativePath", "Blocks/Blocks.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABlocks_Statics::NewProp_HierarchicalInstanced_MetaData[] = {
		{ "Category", "Root" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Blocks/Blocks.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABlocks_Statics::NewProp_HierarchicalInstanced = { "HierarchicalInstanced", nullptr, (EPropertyFlags)0x001000000008000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABlocks, HierarchicalInstanced), Z_Construct_UClass_UHierarchicalInstancedStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABlocks_Statics::NewProp_HierarchicalInstanced_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABlocks_Statics::NewProp_HierarchicalInstanced_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABlocks_Statics::NewProp_Material_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ModuleRelativePath", "Blocks/Blocks.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABlocks_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABlocks, Material), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABlocks_Statics::NewProp_Material_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABlocks_Statics::NewProp_Material_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABlocks_Statics::NewProp_BlockScale_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ModuleRelativePath", "Blocks/Blocks.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ABlocks_Statics::NewProp_BlockScale = { "BlockScale", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABlocks, BlockScale), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ABlocks_Statics::NewProp_BlockScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABlocks_Statics::NewProp_BlockScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABlocks_Statics::NewProp_CoutX_MetaData[] = {
		{ "Category", "ConstructorBlocks" },
		{ "ModuleRelativePath", "Blocks/Blocks.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_ABlocks_Statics::NewProp_CoutX = { "CoutX", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABlocks, CoutX), METADATA_PARAMS(Z_Construct_UClass_ABlocks_Statics::NewProp_CoutX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABlocks_Statics::NewProp_CoutX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABlocks_Statics::NewProp_CoutY_MetaData[] = {
		{ "Category", "ConstructorBlocks" },
		{ "ModuleRelativePath", "Blocks/Blocks.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_ABlocks_Statics::NewProp_CoutY = { "CoutY", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABlocks, CoutY), METADATA_PARAMS(Z_Construct_UClass_ABlocks_Statics::NewProp_CoutY_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABlocks_Statics::NewProp_CoutY_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABlocks_Statics::NewProp_Padding_MetaData[] = {
		{ "Category", "ConstructorBlocks" },
		{ "ModuleRelativePath", "Blocks/Blocks.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABlocks_Statics::NewProp_Padding = { "Padding", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABlocks, Padding), METADATA_PARAMS(Z_Construct_UClass_ABlocks_Statics::NewProp_Padding_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABlocks_Statics::NewProp_Padding_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABlocks_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABlocks_Statics::NewProp_HierarchicalInstanced,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABlocks_Statics::NewProp_Material,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABlocks_Statics::NewProp_BlockScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABlocks_Statics::NewProp_CoutX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABlocks_Statics::NewProp_CoutY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABlocks_Statics::NewProp_Padding,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABlocks_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABlocks>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABlocks_Statics::ClassParams = {
		&ABlocks::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ABlocks_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ABlocks_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABlocks_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABlocks_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABlocks()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABlocks_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABlocks, 1403201569);
	template<> ARCANOID_API UClass* StaticClass<ABlocks>()
	{
		return ABlocks::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABlocks(Z_Construct_UClass_ABlocks, &ABlocks::StaticClass, TEXT("/Script/Arcanoid"), TEXT("ABlocks"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABlocks);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
