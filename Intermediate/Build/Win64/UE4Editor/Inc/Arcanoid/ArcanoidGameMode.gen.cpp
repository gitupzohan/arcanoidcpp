// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Arcanoid/Game/ArcanoidGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeArcanoidGameMode() {}
// Cross Module References
	ARCANOID_API UClass* Z_Construct_UClass_AArcanoidGameMode_NoRegister();
	ARCANOID_API UClass* Z_Construct_UClass_AArcanoidGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameMode();
	UPackage* Z_Construct_UPackage__Script_Arcanoid();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ARCANOID_API UClass* Z_Construct_UClass_ABall_NoRegister();
	ARCANOID_API UClass* Z_Construct_UClass_AExploseBall_NoRegister();
	ARCANOID_API UClass* Z_Construct_UClass_ABlockBase_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AArcanoidGameMode::execCanSpawnExploseBall)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CanSpawnExploseBall();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AArcanoidGameMode::execCanSpawnBall)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CanSpawnBall();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AArcanoidGameMode::execSpawnPlayingBall)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SpawnPlayingBall();
		P_NATIVE_END;
	}
	void AArcanoidGameMode::StaticRegisterNativesAArcanoidGameMode()
	{
		UClass* Class = AArcanoidGameMode::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CanSpawnBall", &AArcanoidGameMode::execCanSpawnBall },
			{ "CanSpawnExploseBall", &AArcanoidGameMode::execCanSpawnExploseBall },
			{ "SpawnPlayingBall", &AArcanoidGameMode::execSpawnPlayingBall },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AArcanoidGameMode_CanSpawnBall_Statics
	{
		struct ArcanoidGameMode_eventCanSpawnBall_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AArcanoidGameMode_CanSpawnBall_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ArcanoidGameMode_eventCanSpawnBall_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AArcanoidGameMode_CanSpawnBall_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ArcanoidGameMode_eventCanSpawnBall_Parms), &Z_Construct_UFunction_AArcanoidGameMode_CanSpawnBall_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AArcanoidGameMode_CanSpawnBall_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AArcanoidGameMode_CanSpawnBall_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArcanoidGameMode_CanSpawnBall_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Game/ArcanoidGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArcanoidGameMode_CanSpawnBall_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArcanoidGameMode, nullptr, "CanSpawnBall", nullptr, nullptr, sizeof(ArcanoidGameMode_eventCanSpawnBall_Parms), Z_Construct_UFunction_AArcanoidGameMode_CanSpawnBall_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidGameMode_CanSpawnBall_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArcanoidGameMode_CanSpawnBall_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidGameMode_CanSpawnBall_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArcanoidGameMode_CanSpawnBall()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArcanoidGameMode_CanSpawnBall_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AArcanoidGameMode_CanSpawnExploseBall_Statics
	{
		struct ArcanoidGameMode_eventCanSpawnExploseBall_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AArcanoidGameMode_CanSpawnExploseBall_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ArcanoidGameMode_eventCanSpawnExploseBall_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AArcanoidGameMode_CanSpawnExploseBall_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ArcanoidGameMode_eventCanSpawnExploseBall_Parms), &Z_Construct_UFunction_AArcanoidGameMode_CanSpawnExploseBall_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AArcanoidGameMode_CanSpawnExploseBall_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AArcanoidGameMode_CanSpawnExploseBall_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArcanoidGameMode_CanSpawnExploseBall_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Game/ArcanoidGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArcanoidGameMode_CanSpawnExploseBall_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArcanoidGameMode, nullptr, "CanSpawnExploseBall", nullptr, nullptr, sizeof(ArcanoidGameMode_eventCanSpawnExploseBall_Parms), Z_Construct_UFunction_AArcanoidGameMode_CanSpawnExploseBall_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidGameMode_CanSpawnExploseBall_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArcanoidGameMode_CanSpawnExploseBall_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidGameMode_CanSpawnExploseBall_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArcanoidGameMode_CanSpawnExploseBall()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArcanoidGameMode_CanSpawnExploseBall_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AArcanoidGameMode_SpawnPlayingBall_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArcanoidGameMode_SpawnPlayingBall_Statics::Function_MetaDataParams[] = {
		{ "Comment", "//functions\n" },
		{ "ModuleRelativePath", "Game/ArcanoidGameMode.h" },
		{ "ToolTip", "functions" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArcanoidGameMode_SpawnPlayingBall_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArcanoidGameMode, nullptr, "SpawnPlayingBall", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArcanoidGameMode_SpawnPlayingBall_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidGameMode_SpawnPlayingBall_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArcanoidGameMode_SpawnPlayingBall()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArcanoidGameMode_SpawnPlayingBall_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AArcanoidGameMode_NoRegister()
	{
		return AArcanoidGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AArcanoidGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayingBall_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_PlayingBall;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExploseBall_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ExploseBall;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Block_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Block;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BombMaxCout_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_BombMaxCout;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BallMaxCout_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_BallMaxCout;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AArcanoidGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameMode,
		(UObject* (*)())Z_Construct_UPackage__Script_Arcanoid,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AArcanoidGameMode_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AArcanoidGameMode_CanSpawnBall, "CanSpawnBall" }, // 3593971092
		{ &Z_Construct_UFunction_AArcanoidGameMode_CanSpawnExploseBall, "CanSpawnExploseBall" }, // 1674801393
		{ &Z_Construct_UFunction_AArcanoidGameMode_SpawnPlayingBall, "SpawnPlayingBall" }, // 1796442023
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Game/ArcanoidGameMode.h" },
		{ "ModuleRelativePath", "Game/ArcanoidGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_PlayingBall_MetaData[] = {
		{ "Category", "Actors" },
		{ "Comment", "//property\n" },
		{ "ModuleRelativePath", "Game/ArcanoidGameMode.h" },
		{ "ToolTip", "property" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_PlayingBall = { "PlayingBall", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidGameMode, PlayingBall), Z_Construct_UClass_ABall_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_PlayingBall_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_PlayingBall_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_ExploseBall_MetaData[] = {
		{ "Category", "Actors" },
		{ "ModuleRelativePath", "Game/ArcanoidGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_ExploseBall = { "ExploseBall", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidGameMode, ExploseBall), Z_Construct_UClass_AExploseBall_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_ExploseBall_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_ExploseBall_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_Block_MetaData[] = {
		{ "Category", "Actors" },
		{ "ModuleRelativePath", "Game/ArcanoidGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_Block = { "Block", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidGameMode, Block), Z_Construct_UClass_ABlockBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_Block_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_Block_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_BombMaxCout_MetaData[] = {
		{ "Category", "Special" },
		{ "ModuleRelativePath", "Game/ArcanoidGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_BombMaxCout = { "BombMaxCout", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidGameMode, BombMaxCout), METADATA_PARAMS(Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_BombMaxCout_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_BombMaxCout_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_BallMaxCout_MetaData[] = {
		{ "Category", "Special" },
		{ "ModuleRelativePath", "Game/ArcanoidGameMode.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_BallMaxCout = { "BallMaxCout", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidGameMode, BallMaxCout), METADATA_PARAMS(Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_BallMaxCout_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_BallMaxCout_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AArcanoidGameMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_PlayingBall,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_ExploseBall,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_Block,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_BombMaxCout,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidGameMode_Statics::NewProp_BallMaxCout,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AArcanoidGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AArcanoidGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AArcanoidGameMode_Statics::ClassParams = {
		&AArcanoidGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AArcanoidGameMode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidGameMode_Statics::PropPointers),
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AArcanoidGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AArcanoidGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AArcanoidGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AArcanoidGameMode, 537286102);
	template<> ARCANOID_API UClass* StaticClass<AArcanoidGameMode>()
	{
		return AArcanoidGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AArcanoidGameMode(Z_Construct_UClass_AArcanoidGameMode, &AArcanoidGameMode::StaticClass, TEXT("/Script/Arcanoid"), TEXT("AArcanoidGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AArcanoidGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
