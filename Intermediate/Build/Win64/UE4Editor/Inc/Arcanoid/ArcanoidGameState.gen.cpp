// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Arcanoid/Game/ArcanoidGameState.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeArcanoidGameState() {}
// Cross Module References
	ARCANOID_API UClass* Z_Construct_UClass_AArcanoidGameState_NoRegister();
	ARCANOID_API UClass* Z_Construct_UClass_AArcanoidGameState();
	ENGINE_API UClass* Z_Construct_UClass_AGameState();
	UPackage* Z_Construct_UPackage__Script_Arcanoid();
	ARCANOID_API UClass* Z_Construct_UClass_ABall_NoRegister();
	ARCANOID_API UClass* Z_Construct_UClass_ARacket_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AArcanoidGameState::execIncrementBomb)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->IncrementBomb(Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AArcanoidGameState::execIncrementScore)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->IncrementScore(Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AArcanoidGameState::execIncrementHealth)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->IncrementHealth(Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AArcanoidGameState::execRemoveBall)
	{
		P_GET_OBJECT(ABall,Z_Param_Ball);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveBall(Z_Param_Ball);
		P_NATIVE_END;
	}
	void AArcanoidGameState::StaticRegisterNativesAArcanoidGameState()
	{
		UClass* Class = AArcanoidGameState::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "IncrementBomb", &AArcanoidGameState::execIncrementBomb },
			{ "IncrementHealth", &AArcanoidGameState::execIncrementHealth },
			{ "IncrementScore", &AArcanoidGameState::execIncrementScore },
			{ "RemoveBall", &AArcanoidGameState::execRemoveBall },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AArcanoidGameState_IncrementBomb_Statics
	{
		struct ArcanoidGameState_eventIncrementBomb_Parms
		{
			int32 Value;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_AArcanoidGameState_IncrementBomb_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ArcanoidGameState_eventIncrementBomb_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AArcanoidGameState_IncrementBomb_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AArcanoidGameState_IncrementBomb_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArcanoidGameState_IncrementBomb_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Game/ArcanoidGameState.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArcanoidGameState_IncrementBomb_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArcanoidGameState, nullptr, "IncrementBomb", nullptr, nullptr, sizeof(ArcanoidGameState_eventIncrementBomb_Parms), Z_Construct_UFunction_AArcanoidGameState_IncrementBomb_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidGameState_IncrementBomb_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArcanoidGameState_IncrementBomb_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidGameState_IncrementBomb_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArcanoidGameState_IncrementBomb()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArcanoidGameState_IncrementBomb_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AArcanoidGameState_IncrementHealth_Statics
	{
		struct ArcanoidGameState_eventIncrementHealth_Parms
		{
			int32 Value;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_AArcanoidGameState_IncrementHealth_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ArcanoidGameState_eventIncrementHealth_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AArcanoidGameState_IncrementHealth_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AArcanoidGameState_IncrementHealth_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArcanoidGameState_IncrementHealth_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Game/ArcanoidGameState.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArcanoidGameState_IncrementHealth_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArcanoidGameState, nullptr, "IncrementHealth", nullptr, nullptr, sizeof(ArcanoidGameState_eventIncrementHealth_Parms), Z_Construct_UFunction_AArcanoidGameState_IncrementHealth_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidGameState_IncrementHealth_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArcanoidGameState_IncrementHealth_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidGameState_IncrementHealth_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArcanoidGameState_IncrementHealth()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArcanoidGameState_IncrementHealth_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AArcanoidGameState_IncrementScore_Statics
	{
		struct ArcanoidGameState_eventIncrementScore_Parms
		{
			int32 Value;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_AArcanoidGameState_IncrementScore_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ArcanoidGameState_eventIncrementScore_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AArcanoidGameState_IncrementScore_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AArcanoidGameState_IncrementScore_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArcanoidGameState_IncrementScore_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Game/ArcanoidGameState.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArcanoidGameState_IncrementScore_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArcanoidGameState, nullptr, "IncrementScore", nullptr, nullptr, sizeof(ArcanoidGameState_eventIncrementScore_Parms), Z_Construct_UFunction_AArcanoidGameState_IncrementScore_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidGameState_IncrementScore_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArcanoidGameState_IncrementScore_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidGameState_IncrementScore_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArcanoidGameState_IncrementScore()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArcanoidGameState_IncrementScore_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AArcanoidGameState_RemoveBall_Statics
	{
		struct ArcanoidGameState_eventRemoveBall_Parms
		{
			ABall* Ball;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Ball;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AArcanoidGameState_RemoveBall_Statics::NewProp_Ball = { "Ball", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ArcanoidGameState_eventRemoveBall_Parms, Ball), Z_Construct_UClass_ABall_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AArcanoidGameState_RemoveBall_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AArcanoidGameState_RemoveBall_Statics::NewProp_Ball,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArcanoidGameState_RemoveBall_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Game/ArcanoidGameState.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArcanoidGameState_RemoveBall_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArcanoidGameState, nullptr, "RemoveBall", nullptr, nullptr, sizeof(ArcanoidGameState_eventRemoveBall_Parms), Z_Construct_UFunction_AArcanoidGameState_RemoveBall_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidGameState_RemoveBall_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArcanoidGameState_RemoveBall_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidGameState_RemoveBall_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArcanoidGameState_RemoveBall()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArcanoidGameState_RemoveBall_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AArcanoidGameState_NoRegister()
	{
		return AArcanoidGameState::StaticClass();
	}
	struct Z_Construct_UClass_AArcanoidGameState_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AllBalls_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllBalls_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AllBalls;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BombCurrentCout_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_BombCurrentCout;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Health_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Health;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Score_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Score;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayingBall_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlayingBall;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Racket_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Racket;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AArcanoidGameState_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameState,
		(UObject* (*)())Z_Construct_UPackage__Script_Arcanoid,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AArcanoidGameState_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AArcanoidGameState_IncrementBomb, "IncrementBomb" }, // 2599548107
		{ &Z_Construct_UFunction_AArcanoidGameState_IncrementHealth, "IncrementHealth" }, // 244947365
		{ &Z_Construct_UFunction_AArcanoidGameState_IncrementScore, "IncrementScore" }, // 691881580
		{ &Z_Construct_UFunction_AArcanoidGameState_RemoveBall, "RemoveBall" }, // 2857302453
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidGameState_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Game/ArcanoidGameState.h" },
		{ "ModuleRelativePath", "Game/ArcanoidGameState.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_AllBalls_Inner = { "AllBalls", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ABall_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_AllBalls_MetaData[] = {
		{ "Category", "ArcanoidGameState" },
		{ "ModuleRelativePath", "Game/ArcanoidGameState.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_AllBalls = { "AllBalls", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidGameState, AllBalls), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_AllBalls_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_AllBalls_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_BombCurrentCout_MetaData[] = {
		{ "Category", "ArcanoidGameState" },
		{ "ModuleRelativePath", "Game/ArcanoidGameState.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_BombCurrentCout = { "BombCurrentCout", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidGameState, BombCurrentCout), METADATA_PARAMS(Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_BombCurrentCout_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_BombCurrentCout_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_Health_MetaData[] = {
		{ "Category", "ArcanoidGameState" },
		{ "ModuleRelativePath", "Game/ArcanoidGameState.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_Health = { "Health", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidGameState, Health), METADATA_PARAMS(Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_Health_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_Health_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_Score_MetaData[] = {
		{ "Category", "ArcanoidGameState" },
		{ "ModuleRelativePath", "Game/ArcanoidGameState.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_Score = { "Score", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidGameState, Score), METADATA_PARAMS(Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_Score_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_Score_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_PlayingBall_MetaData[] = {
		{ "Category", "Actors" },
		{ "ModuleRelativePath", "Game/ArcanoidGameState.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_PlayingBall = { "PlayingBall", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidGameState, PlayingBall), Z_Construct_UClass_ABall_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_PlayingBall_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_PlayingBall_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_Racket_MetaData[] = {
		{ "Category", "Actors" },
		{ "ModuleRelativePath", "Game/ArcanoidGameState.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_Racket = { "Racket", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidGameState, Racket), Z_Construct_UClass_ARacket_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_Racket_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_Racket_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AArcanoidGameState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_AllBalls_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_AllBalls,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_BombCurrentCout,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_Health,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_Score,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_PlayingBall,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidGameState_Statics::NewProp_Racket,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AArcanoidGameState_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AArcanoidGameState>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AArcanoidGameState_Statics::ClassParams = {
		&AArcanoidGameState::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AArcanoidGameState_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidGameState_Statics::PropPointers),
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_AArcanoidGameState_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidGameState_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AArcanoidGameState()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AArcanoidGameState_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AArcanoidGameState, 4267392248);
	template<> ARCANOID_API UClass* StaticClass<AArcanoidGameState>()
	{
		return AArcanoidGameState::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AArcanoidGameState(Z_Construct_UClass_AArcanoidGameState, &AArcanoidGameState::StaticClass, TEXT("/Script/Arcanoid"), TEXT("AArcanoidGameState"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AArcanoidGameState);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
