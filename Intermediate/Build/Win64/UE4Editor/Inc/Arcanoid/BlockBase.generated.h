// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef ARCANOID_BlockBase_generated_h
#error "BlockBase.generated.h already included, missing '#pragma once' in BlockBase.h"
#endif
#define ARCANOID_BlockBase_generated_h

#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_SPARSE_DATA
#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execBlockHit);


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execBlockHit);


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABlockBase(); \
	friend struct Z_Construct_UClass_ABlockBase_Statics; \
public: \
	DECLARE_CLASS(ABlockBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(ABlockBase)


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_INCLASS \
private: \
	static void StaticRegisterNativesABlockBase(); \
	friend struct Z_Construct_UClass_ABlockBase_Statics; \
public: \
	DECLARE_CLASS(ABlockBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(ABlockBase)


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABlockBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABlockBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABlockBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABlockBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABlockBase(ABlockBase&&); \
	NO_API ABlockBase(const ABlockBase&); \
public:


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABlockBase(ABlockBase&&); \
	NO_API ABlockBase(const ABlockBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABlockBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABlockBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABlockBase)


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_PRIVATE_PROPERTY_OFFSET
#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_10_PROLOG
#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_RPC_WRAPPERS \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_INCLASS \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_INCLASS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARCANOID_API UClass* StaticClass<class ABlockBase>();

#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_SPARSE_DATA
#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_RPC_WRAPPERS
#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_RPC_WRAPPERS_NO_PURE_DECLS
#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAUndestructBlock(); \
	friend struct Z_Construct_UClass_AUndestructBlock_Statics; \
public: \
	DECLARE_CLASS(AUndestructBlock, ABlockBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(AUndestructBlock)


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_INCLASS \
private: \
	static void StaticRegisterNativesAUndestructBlock(); \
	friend struct Z_Construct_UClass_AUndestructBlock_Statics; \
public: \
	DECLARE_CLASS(AUndestructBlock, ABlockBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(AUndestructBlock)


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUndestructBlock(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUndestructBlock) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUndestructBlock); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUndestructBlock); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUndestructBlock(AUndestructBlock&&); \
	NO_API AUndestructBlock(const AUndestructBlock&); \
public:


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUndestructBlock() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUndestructBlock(AUndestructBlock&&); \
	NO_API AUndestructBlock(const AUndestructBlock&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUndestructBlock); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUndestructBlock); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AUndestructBlock)


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_PRIVATE_PROPERTY_OFFSET
#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_35_PROLOG
#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_RPC_WRAPPERS \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_INCLASS \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_INCLASS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_38_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARCANOID_API UClass* StaticClass<class AUndestructBlock>();

#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_SPARSE_DATA
#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_RPC_WRAPPERS
#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_RPC_WRAPPERS_NO_PURE_DECLS
#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABombBlock(); \
	friend struct Z_Construct_UClass_ABombBlock_Statics; \
public: \
	DECLARE_CLASS(ABombBlock, ABlockBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(ABombBlock)


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_INCLASS \
private: \
	static void StaticRegisterNativesABombBlock(); \
	friend struct Z_Construct_UClass_ABombBlock_Statics; \
public: \
	DECLARE_CLASS(ABombBlock, ABlockBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(ABombBlock)


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABombBlock(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABombBlock) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABombBlock); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABombBlock); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABombBlock(ABombBlock&&); \
	NO_API ABombBlock(const ABombBlock&); \
public:


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABombBlock() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABombBlock(ABombBlock&&); \
	NO_API ABombBlock(const ABombBlock&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABombBlock); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABombBlock); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABombBlock)


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_PRIVATE_PROPERTY_OFFSET
#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_54_PROLOG
#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_RPC_WRAPPERS \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_INCLASS \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_INCLASS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Blocks_BlockBase_h_57_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARCANOID_API UClass* StaticClass<class ABombBlock>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcanoid_Source_Arcanoid_Blocks_BlockBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
