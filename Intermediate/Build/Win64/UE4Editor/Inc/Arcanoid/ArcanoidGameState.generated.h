// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ABall;
#ifdef ARCANOID_ArcanoidGameState_generated_h
#error "ArcanoidGameState.generated.h already included, missing '#pragma once' in ArcanoidGameState.h"
#endif
#define ARCANOID_ArcanoidGameState_generated_h

#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_SPARSE_DATA
#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execIncrementBomb); \
	DECLARE_FUNCTION(execIncrementScore); \
	DECLARE_FUNCTION(execIncrementHealth); \
	DECLARE_FUNCTION(execRemoveBall);


#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execIncrementBomb); \
	DECLARE_FUNCTION(execIncrementScore); \
	DECLARE_FUNCTION(execIncrementHealth); \
	DECLARE_FUNCTION(execRemoveBall);


#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAArcanoidGameState(); \
	friend struct Z_Construct_UClass_AArcanoidGameState_Statics; \
public: \
	DECLARE_CLASS(AArcanoidGameState, AGameState, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(AArcanoidGameState)


#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAArcanoidGameState(); \
	friend struct Z_Construct_UClass_AArcanoidGameState_Statics; \
public: \
	DECLARE_CLASS(AArcanoidGameState, AGameState, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), NO_API) \
	DECLARE_SERIALIZER(AArcanoidGameState)


#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AArcanoidGameState(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AArcanoidGameState) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArcanoidGameState); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArcanoidGameState); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArcanoidGameState(AArcanoidGameState&&); \
	NO_API AArcanoidGameState(const AArcanoidGameState&); \
public:


#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArcanoidGameState(AArcanoidGameState&&); \
	NO_API AArcanoidGameState(const AArcanoidGameState&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArcanoidGameState); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArcanoidGameState); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AArcanoidGameState)


#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_PRIVATE_PROPERTY_OFFSET
#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_14_PROLOG
#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_RPC_WRAPPERS \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_INCLASS \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_INCLASS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARCANOID_API UClass* StaticClass<class AArcanoidGameState>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcanoid_Source_Arcanoid_Game_ArcanoidGameState_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
