// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Arcanoid/Game/ArcanoidPlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeArcanoidPlayerController() {}
// Cross Module References
	ARCANOID_API UClass* Z_Construct_UClass_AArcanoidPlayerController_NoRegister();
	ARCANOID_API UClass* Z_Construct_UClass_AArcanoidPlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_Arcanoid();
// End Cross Module References
	void AArcanoidPlayerController::StaticRegisterNativesAArcanoidPlayerController()
	{
	}
	UClass* Z_Construct_UClass_AArcanoidPlayerController_NoRegister()
	{
		return AArcanoidPlayerController::StaticClass();
	}
	struct Z_Construct_UClass_AArcanoidPlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AArcanoidPlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_Arcanoid,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPlayerController_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Game/ArcanoidPlayerController.h" },
		{ "ModuleRelativePath", "Game/ArcanoidPlayerController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AArcanoidPlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AArcanoidPlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AArcanoidPlayerController_Statics::ClassParams = {
		&AArcanoidPlayerController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_AArcanoidPlayerController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AArcanoidPlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AArcanoidPlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AArcanoidPlayerController, 92045853);
	template<> ARCANOID_API UClass* StaticClass<AArcanoidPlayerController>()
	{
		return AArcanoidPlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AArcanoidPlayerController(Z_Construct_UClass_AArcanoidPlayerController, &AArcanoidPlayerController::StaticClass, TEXT("/Script/Arcanoid"), TEXT("AArcanoidPlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AArcanoidPlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
