// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARCANOID_ArcanoidGameMode_generated_h
#error "ArcanoidGameMode.generated.h already included, missing '#pragma once' in ArcanoidGameMode.h"
#endif
#define ARCANOID_ArcanoidGameMode_generated_h

#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_SPARSE_DATA
#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCanSpawnExploseBall); \
	DECLARE_FUNCTION(execCanSpawnBall); \
	DECLARE_FUNCTION(execSpawnPlayingBall);


#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCanSpawnExploseBall); \
	DECLARE_FUNCTION(execCanSpawnBall); \
	DECLARE_FUNCTION(execSpawnPlayingBall);


#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAArcanoidGameMode(); \
	friend struct Z_Construct_UClass_AArcanoidGameMode_Statics; \
public: \
	DECLARE_CLASS(AArcanoidGameMode, AGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), ARCANOID_API) \
	DECLARE_SERIALIZER(AArcanoidGameMode)


#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAArcanoidGameMode(); \
	friend struct Z_Construct_UClass_AArcanoidGameMode_Statics; \
public: \
	DECLARE_CLASS(AArcanoidGameMode, AGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Arcanoid"), ARCANOID_API) \
	DECLARE_SERIALIZER(AArcanoidGameMode)


#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ARCANOID_API AArcanoidGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AArcanoidGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ARCANOID_API, AArcanoidGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArcanoidGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ARCANOID_API AArcanoidGameMode(AArcanoidGameMode&&); \
	ARCANOID_API AArcanoidGameMode(const AArcanoidGameMode&); \
public:


#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ARCANOID_API AArcanoidGameMode(AArcanoidGameMode&&); \
	ARCANOID_API AArcanoidGameMode(const AArcanoidGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ARCANOID_API, AArcanoidGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArcanoidGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AArcanoidGameMode)


#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_PRIVATE_PROPERTY_OFFSET
#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_12_PROLOG
#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_RPC_WRAPPERS \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_INCLASS \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_SPARSE_DATA \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_INCLASS_NO_PURE_DECLS \
	Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARCANOID_API UClass* StaticClass<class AArcanoidGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcanoid_Source_Arcanoid_Game_ArcanoidGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
